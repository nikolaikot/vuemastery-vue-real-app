# real-world-vue

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### sets and turns on json-server and tells it to watch our db.json file, which is our mock database.

```
npm install -g json-server
json-server --watch db.json
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
